package com.example.retrofit.util

object Constants {

    const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"

    //Endpoints
    const val LOGIN = "users/auth/sign_in"

}