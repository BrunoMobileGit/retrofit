package com.example.retrofit.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.retrofit.R
import com.example.retrofit.repository.Repository
import com.example.retrofit.viewmodel.LoginViewModel
import com.example.retrofit.viewmodel.ViewModelFactory

class MainActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel // Declara o ViewModel
    val repository = Repository() // Inicializa o repositorio
    val viewModelFactory = ViewModelFactory(repository) // Inicializa o viewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Inicializa o ViewModel
        loginViewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
        //Passa as Credenciais
        loginViewModel.login("testeapple@ioasys.com.br","12341234")

    }
}