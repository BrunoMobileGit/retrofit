package com.example.retrofit.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.retrofit.data.api.CallbackApi
import com.example.retrofit.repository.Repository
import okhttp3.ResponseBody

class LoginViewModel( private val repository: Repository): ViewModel(){

    companion object{
        val repositoryFactory = Repository()
        val viewModelFactory = ViewModelFactory(repositoryFactory)
    }

    fun login(email: String, password: String){
        repository.login(email, password, object: CallbackApi {
            override fun onSuccess(item: String) {
               Log.d("TAG", "onResponse: $item")
            }

            override fun onFailure(item: ResponseBody?) {
               Log.d("TAG", "onResponse: $item")
            }

        })
    }
}