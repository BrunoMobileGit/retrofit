package com.example.retrofit.repository

import android.util.Log
import com.example.retrofit.data.api.CallbackApi
import com.example.retrofit.data.api.RetrofitInstance
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {

    fun login(email: String, password: String, listener: CallbackApi){
        val call = RetrofitInstance.api.login(email,password)

        call.enqueue(object: Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
               Log.d("TAG", "onFailure: teste")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful){
                    listener.onSuccess(
                        "\n Access-token: " + response.headers()["access-token"].toString()
                        + "\n client: " + response.headers()["client"].toString()
                        + "\n uid: " + response.headers()["uid"].toString()
                    )
                }else{
                    listener.onFailure(response.body())
                }
            }
        })
    }



}