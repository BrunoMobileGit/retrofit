package com.example.retrofit.data.api

import okhttp3.ResponseBody

interface CallbackApi {
    fun onSuccess(item: String)
    fun onFailure(item: ResponseBody?)
}