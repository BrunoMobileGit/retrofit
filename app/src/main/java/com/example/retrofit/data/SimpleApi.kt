package com.example.retrofit.data.oauth

import com.example.retrofit.util.Constants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface SimpleApi {

    //Endpoint
    @POST(Constants.LOGIN)
    fun login(
        @Query("email") email: String,
        @Query("password") password: String
    ): Call<ResponseBody>

}