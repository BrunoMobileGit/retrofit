package com.example.retrofit.data.api

import com.example.retrofit.data.oauth.SimpleApi
import com.example.retrofit.util.Constants.BASE_URL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//Classe Responsavel por Inicializar o Interceptor e a Api
object RetrofitInstance {

    //Inicialia o Interceptor
    private val cliente = OkHttpClient.Builder().apply {
        addInterceptor(MyInterceptor())
    }.build()

    //Captura o link da Api
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(cliente)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    //Inicializa a Api
    val api: SimpleApi by lazy{
        retrofit.create(SimpleApi::class.java)
    }

}