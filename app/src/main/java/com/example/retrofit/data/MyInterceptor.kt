package com.example.retrofit.data.api

import okhttp3.Interceptor
import okhttp3.Response

//Passa os Headers
class MyInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("Content-Type", "application/json")
            .addHeader("access-token", "12345678")
            .addHeader("client", "client")
            .addHeader("uid", "uid")
            .build()
        return chain.proceed(request)
    }


}